# Car Information Homework on Spring boot and AngularJS

API are created using Spring boot and small front end is written using AngularJS

## Installation

For server side Installation:

```bash
mvn clean install
mvn spring-boot:run
```

## Usage

```browser
Please open http://localhost:8080 on browser
```

## API End Points
```API
Users:
GET http://localhost:8080/api/v1/user/get - get first page
GET http://localhost:8080/api/v1/user/get/page/{pageNo} - get x page
GET http://localhost:8080/api/v1/user/get/{userId} - get user by Id
POST http://localhost:8080/api/v1/user/get/page/{pageNo} - get filtered and sorted users
request json : {"query":"z", "filter":"name", "sortDirection":"DESC"}

Cars:
GET http://localhost:8080/api/v1/car/get - get first page
GET http://localhost:8080/api/v1/car/get/page/{pageNo} - get x page
GET http://localhost:8080/api/v1/car/get/{userId} - get car by Id
POST http://localhost:8080/api/v1/car/get/page/{pageNo} - get filtered and sorted car
request json : {"query":"z", "filter":"make/model", "sortDirection":"DESC"}
```

## Filter and sorted method
I chose to have server side filtering and sorting along with pagination. I usually prefer server side operations to ensure consistent performance for large databases. Server side operations also give most recent and accurate data.

## Client-side (Front-end)

Client side code is written separately. It can be accesses using below gitlab repository.

```git client
https://gitlab.com/homework-est/cars-client.git
``` 

Please fire below command on above repository to generate compiled code. Once done then all files under 'dist' folder should be copied to resources/static folder of this project. 

```bash
ng build --prod
``` 

## Future Tasks
The Exception handling is not yet implemented. This is very crucial next step to be accomplished. Further client side unit tests are also on roadmap.

## License
[MIT](https://choosealicense.com/licenses/mit/)