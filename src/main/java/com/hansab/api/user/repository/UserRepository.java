package com.hansab.api.user.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hansab.api.user.model.User;


@Repository("userRepository")
@Scope("prototype")
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
	
	Page<User> findAll(Pageable pageable);
	User findByUserId(long userId);
	Page<User> findByNameContainingIgnoreCase(String name, Pageable pageable);
	
	
}

