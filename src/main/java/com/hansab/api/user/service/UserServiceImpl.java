package com.hansab.api.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hansab.api.user.model.CustomUserResult;
import com.hansab.api.user.model.User;
import com.hansab.api.user.repository.UserRepository;
import com.hansab.api.utility.Constants;
import com.hansab.api.utility.SearchDBParams;
import com.hansab.api.utility.Utils;

import lombok.extern.slf4j.Slf4j;


@Service
@Scope("prototype")
@Slf4j
public class UserServiceImpl implements UserService {
	
	@Autowired
    private UserRepository userRepository;
	
	
	public CustomUserResult getUsers(SearchDBParams searchDBParams){
		
		return buildCustomUserResult(userRepository.
										findAll(getPageable(searchDBParams))
									);
	}
	
	public User findByUserId(long userId){
		
		return userRepository.findByUserId(userId);
	}
	
	public CustomUserResult getFilteredUsers(SearchDBParams searchDBParams){
			
		if(searchDBParams.getFilter().equalsIgnoreCase("name")) {
			
			Page<User> pagedUsers = userRepository.findByNameContainingIgnoreCase(searchDBParams.getQuery(), getPageable(searchDBParams));
			return buildCustomUserResult(pagedUsers);
		}
		else
			return getUsers(searchDBParams);
		
	}
	
	/*************************** private supporting methods ********************************************/
	private Pageable getPageable(SearchDBParams searchDBParams) {
		
		Pageable pageable;
		Sort sort = Utils.getSort(searchDBParams);
		
		if(sort == null)
			pageable = PageRequest.of(Utils.ensurePositiveValues(searchDBParams.getPage()-1), Constants.PAGE_SIZE);
		else
			pageable = PageRequest.of(Utils.ensurePositiveValues(searchDBParams.getPage()-1), Constants.PAGE_SIZE, sort);
		
		return pageable;
		
	}
	
	private CustomUserResult buildCustomUserResult(Page<User> pagedUsers) {
		
		return CustomUserResult
				.builder()
				.pageSize(pagedUsers.getSize())
				.page(pagedUsers.getNumber()+1)
				.listUser(pagedUsers.getContent())
				.total(pagedUsers.getTotalPages())
				.build();
		
	}
	
}

