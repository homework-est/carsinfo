package com.hansab.api.user.service;

import com.hansab.api.user.model.CustomUserResult;
import com.hansab.api.user.model.User;
import com.hansab.api.utility.SearchDBParams;


public interface UserService {
	
	public CustomUserResult getUsers(SearchDBParams searchDBParams);
	public User findByUserId(long userId);
	public CustomUserResult getFilteredUsers(SearchDBParams searchDBParams);
	
}

