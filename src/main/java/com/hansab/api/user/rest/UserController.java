package com.hansab.api.user.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hansab.api.user.model.CustomUserResult;
import com.hansab.api.user.model.User;
import com.hansab.api.utility.SearchDBParams;
import com.hansab.api.utility.Utils;
import com.hansab.api.user.service.UserService;
import com.hansab.api.utility.FilterRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@CrossOrigin
@Controller
@Slf4j
@RequestMapping(UserController.API_ROUTE_MAPPING)
public class UserController {
	
	static final String API_ROUTE_MAPPING = "/api/v1/user";
	
	@Autowired
	UserService userService;
	
	@GetMapping("/get")
    public @ResponseBody ResponseEntity<CustomUserResult> getUsers(){
		
		CustomUserResult customUserResult = userService.getUsers(SearchDBParams
																.builder()
																.page(0)
																.build());
		if(customUserResult.getListUser().size()<=0){
            log.debug("Users not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customUserResult, HttpStatus.OK); 
	}
	
	@GetMapping("/get/page/{pageNo}")
    public @ResponseBody ResponseEntity<CustomUserResult> getPagedUsers(@PathVariable int pageNo){
		
		CustomUserResult customUserResult = userService.getUsers(SearchDBParams
																	.builder()
																	.page(pageNo)
																	.build());
		if(customUserResult.getListUser().size()<=0){
			log.debug("Users not found");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customUserResult, HttpStatus.OK);
	}
	
	@GetMapping("/get/{userId}")
    public @ResponseBody ResponseEntity<User> getUserById(@PathVariable long userId){
		
		User user = userService.findByUserId(userId);
		
		if(user == null) 
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<>(user, HttpStatus.OK);
			
	}
	
	@PostMapping("/get/page/{pageNo}")
    public @ResponseBody ResponseEntity<CustomUserResult> getFilteredUsers(@PathVariable int pageNo, @RequestBody FilterRequest filterRequest){
		
		CustomUserResult customUserResult = userService.getFilteredUsers(SearchDBParams
																			.builder()
																			.query(Utils.checkNullValueForString(filterRequest.getQuery()))
																			.filter(Utils.checkNullValueForString(filterRequest.getFilter()))
																			.sortDirection(Utils.checkNullValueForString(filterRequest.getSortDirection()))
																			.page(Utils.parseInt(pageNo))
																			.build());
		if(customUserResult.getListUser().size()<=0){
			log.debug("Users not found");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customUserResult, HttpStatus.OK);
	}
}
