package com.hansab.api.utility;

import org.springframework.data.domain.Sort;

public class Utils {

	/**
	 * This method returns the value of the parameter to a blank value from null
	 * value
	 * 
	 * @param parameter String containing parameter
	 * @return String
	 */
	public static String checkNullValueForString(Object obj) {
		return ((obj == null) ? "" : obj.toString().trim());
	}

	/**
	 * This method returns 0 for minus values value
	 * 
	 * @param parameter String containing parameter
	 * @return String
	 */
	public static int ensurePositiveValues(int num) {
		return ((num < 0) ? 0 : num);
	}

	/**
	 * This method returns the value of the parameter to a zero value from null
	 * value
	 * 
	 * @param parameter String containing parameter
	 * @return String
	 */
	public static String checkNullValueForInt(Object obj) {
		String retVal = "0";
		if (obj == null) {
			retVal = "0";
		} else {
			retVal = obj.toString();
			if (retVal.length() == 0) {
				retVal = "0";
			}
		}
		return retVal;
	}

	public static int parseInt(Object obj) {
		int numValue = 0;
		try {
			if (obj != null && obj.toString().trim().length() > 0) {
				numValue = Integer.parseInt(obj.toString().trim());
			}
		} catch (NumberFormatException nfe) {
			System.out.println("common.Utils.java ::Exception while converting a string value to integer");
			nfe.printStackTrace();
			return numValue;
		}
		return numValue;
	}

	public static Long parseLong(Object obj) {
		Long numValue = 0L;
		try {
			if (obj != null && obj.toString().trim().length() > 0) {
				numValue = Long.parseLong(getPlainString(obj.toString().trim()));
			}
		} catch (NumberFormatException nfe) {
			System.out.println("common.Utils.java ::Exception while converting a string value to Long");
			nfe.printStackTrace();
			return numValue;
		}
		return numValue;
	}

	public static double parseDouble(Object obj) {
		double numValue = 0.0;
		try {
			if (obj != null && obj.toString().trim().length() > 0) {
				numValue = Double.parseDouble(obj.toString().trim());
			}
		} catch (NumberFormatException nfe) {
			System.out.println("common.Utils.java ::Exception while converting a string value to integer");
			nfe.printStackTrace();
			return numValue;
		}
		return numValue;
	}
	
	public static String getPlainString(Object obj) {
		String retString = "";
		if (obj != null) {
			retString = obj.toString().trim();
			retString = retString.replaceAll(" ", "");
			retString = retString.replaceAll("_", "");
			retString = retString.replaceAll(",", "");
		}
		return retString;
	}

	public static Sort getSort(SearchDBParams searchDBParams) {

		Sort sort = null;
		
		if(Utils.checkNullValueForString(searchDBParams.getFilter()).equalsIgnoreCase(""))
			return sort;

		if(Utils.checkNullValueForString(searchDBParams.getSortDirection()).equalsIgnoreCase(Constants.DESC))
			sort = Sort.by(Sort.Direction.DESC, searchDBParams.getFilter());
		else
			sort = Sort.by(Sort.Direction.ASC, searchDBParams.getFilter());
		
		return sort;
	}
}
