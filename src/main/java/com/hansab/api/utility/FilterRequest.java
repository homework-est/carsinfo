package com.hansab.api.utility;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Component
@Scope("prototype")
public class FilterRequest {
	
	private String query = "";
	private String filter = "";
	private String sortDirection = "";
}
