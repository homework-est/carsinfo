package com.hansab.api.car.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hansab.api.car.model.Car;
import com.hansab.api.car.model.CustomCarResult;
import com.hansab.api.car.service.CarService;
import com.hansab.api.utility.FilterRequest;
import com.hansab.api.utility.SearchDBParams;
import com.hansab.api.utility.Utils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@CrossOrigin
@Controller
@Slf4j
@RequestMapping(CarController.API_ROUTE_MAPPING)
public class CarController {
	
	static final String API_ROUTE_MAPPING = "/api/v1/car";
	
	@Autowired
	CarService carService;
	
	@GetMapping("/get")
    public @ResponseBody ResponseEntity<CustomCarResult> getCars(){
		
		CustomCarResult customCarResult = carService.getCars(SearchDBParams
																.builder()
																.page(0)
																.build());
		if(customCarResult.getListCar().size()<=0){
            log.debug("Cars not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customCarResult, HttpStatus.OK); 
	}
	
	@GetMapping("/get/page/{pageNo}")
    public @ResponseBody ResponseEntity<CustomCarResult> getPagedCars(@PathVariable int pageNo){
		
		CustomCarResult customCarResult = carService.getCars(SearchDBParams
																	.builder()
																	.page(pageNo)
																	.build());
		if(customCarResult.getListCar().size()<=0){
			log.debug("Cars not found");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customCarResult, HttpStatus.OK);
	}
	
	@GetMapping("/get/{carId}")
    public @ResponseBody ResponseEntity<Car> getCarById(@PathVariable long carId){
		
		Car car = carService.findByCarId(carId);
		
		if(car == null) 
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<>(car, HttpStatus.OK);
			
	}
	
	@PostMapping("/get/page/{pageNo}")
    public @ResponseBody ResponseEntity<CustomCarResult> getFilteredCars(@PathVariable int pageNo, @RequestBody FilterRequest filterRequest){
		
		CustomCarResult customCarResult = carService.getFilteredCars(SearchDBParams
																			.builder()
																			.query(Utils.checkNullValueForString(filterRequest.getQuery()))
																			.filter(Utils.checkNullValueForString(filterRequest.getFilter()))
																			.sortDirection(Utils.checkNullValueForString(filterRequest.getSortDirection()))
																			.page(Utils.parseInt(pageNo))
																			.build());
		if(customCarResult.getListCar().size()<=0){
			log.debug("Cars not found");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
			return new ResponseEntity<>(customCarResult, HttpStatus.OK);
	}
	
}
