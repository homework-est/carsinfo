package com.hansab.api.car.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Builder
@Component
@Scope("prototype")
public class CustomCarResult {
	
	@JsonProperty("cars")
	private List<Car> listCar;
	private int pageSize = 0;
	private int page = 0;
	private long total = 0;
}
