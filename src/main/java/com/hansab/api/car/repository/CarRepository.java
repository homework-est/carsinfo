package com.hansab.api.car.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hansab.api.car.model.Car;
import com.hansab.api.user.model.User;


@Repository("carRepository")
@Scope("prototype")
public interface CarRepository extends PagingAndSortingRepository<Car, Long>{
	
	Page<Car> findAll(Pageable pageable);
	Car findByCarId(long carId);
	Page<Car> findByModelContainingIgnoreCase(String model, Pageable pageable);
	Page<Car> findByMakeContainingIgnoreCase(String model, Pageable pageable);
}

