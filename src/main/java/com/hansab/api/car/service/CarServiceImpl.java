package com.hansab.api.car.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hansab.api.car.model.Car;
import com.hansab.api.car.model.CustomCarResult;
import com.hansab.api.car.repository.CarRepository;
import com.hansab.api.utility.Constants;
import com.hansab.api.utility.SearchDBParams;
import com.hansab.api.utility.Utils;


@Service
@Scope("prototype")
public class CarServiceImpl implements CarService{
	
	@Autowired
    private CarRepository carRepository;
	
	
	public CustomCarResult getCars(SearchDBParams searchDBParams){
		
		return buildCustomCarResult(carRepository.findAll(getPageable(searchDBParams)));
	}
	
	public Car findByCarId(long carId){
		
		return carRepository.findByCarId(carId);
	}
	
	public CustomCarResult getFilteredCars(SearchDBParams searchDBParams){
			
		if(searchDBParams.getFilter().equalsIgnoreCase("model")) {
			
			return buildCustomCarResult(carRepository.findByModelContainingIgnoreCase(searchDBParams.getQuery(), 
																						getPageable(searchDBParams))
										);
		}
		else if(searchDBParams.getFilter().equalsIgnoreCase("make")) {
			
			return buildCustomCarResult(carRepository.findByMakeContainingIgnoreCase(searchDBParams.getQuery(), 
																					getPageable(searchDBParams))
										);
		}
		else
			return getCars(searchDBParams);
		
	}
	
	/*************************** private supporting methods ********************************************/
	private Pageable getPageable(SearchDBParams searchDBParams) {
		
		Sort sort = Utils.getSort(searchDBParams);
		
		if(sort == null)
			return PageRequest.of(Utils.ensurePositiveValues(searchDBParams.getPage()-1), 
								Constants.PAGE_SIZE);
		else
			return PageRequest.of(Utils.ensurePositiveValues(searchDBParams.getPage()-1), 
								Constants.PAGE_SIZE, sort);	
	}
	
	
	private CustomCarResult buildCustomCarResult(Page<Car> pagedCars) {
		
		return CustomCarResult
				.builder()
				.pageSize(pagedCars.getSize())
				.page(pagedCars.getNumber()+1)
				.listCar(pagedCars.getContent())
				.total(pagedCars.getTotalPages())
				.build();
	}
	
}

