package com.hansab.api.car.service;

import com.hansab.api.car.model.Car;
import com.hansab.api.car.model.CustomCarResult;
import com.hansab.api.utility.SearchDBParams;


public interface CarService {
	
	public CustomCarResult getCars(SearchDBParams searchDBParams);
	public Car findByCarId(long carId);
	public CustomCarResult getFilteredCars(SearchDBParams searchDBParams);
	
}

