package com.hansab.api.user.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hansab.api.ApiApplication;
import com.hansab.api.car.model.Car;
import com.hansab.api.user.model.User;
import com.hansab.api.user.repository.UserRepository;
import com.hansab.api.user.service.UserService;
import com.hansab.api.user.service.UserServiceImpl;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class UserServiceIntegrationTest {
	
	@TestConfiguration
    static class UserServiceImplTestContextConfiguration {
  
        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }
 
    @Autowired
    private UserService userService;
 
    @MockBean
    private UserRepository userRepository;
    
    @Before
    public void setUp() {
    	
    	User dominic = new User();
    	dominic.setUserId(1L);
    	dominic.setName("Dominic");
    	
    	Car v2J9 = new Car();
    	v2J9.setCarId(1L);
    	v2J9.setMake("Isuzu");
    	v2J9.setModel("V2J9");
    	v2J9.setNumberPlate("513XIS");
    	
    	Car d2C1 = new Car();
    	d2C1.setCarId(2L);
    	d2C1.setMake("General Motors");
    	d2C1.setModel("D2C1");
    	d2C1.setNumberPlate("119SVM");
    	
    	dominic.setListCar(new ArrayList<Car>());
    	dominic.getListCar().add(v2J9);
    	dominic.getListCar().add(d2C1);
     
        Mockito.when(userRepository.findByUserId(1))
          .thenReturn(dominic);
    }
	
    
    @Test
    public void findByUserId_test() throws Exception {
		
		User user = userService.findByUserId(1);
		assertThat(user, hasProperty("name", equalTo("Dominic")));
	}
}
