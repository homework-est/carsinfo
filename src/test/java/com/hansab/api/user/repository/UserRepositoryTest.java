package com.hansab.api.user.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hansab.api.ApiApplication;
import com.hansab.api.user.model.User;
import com.hansab.api.user.repository.UserRepository;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class UserRepositoryTest {
	
	@Autowired
    private UserRepository userRepository;
	
	@Test
    public void findAll_test() throws Exception {
		
		Page<User> pagedUser = userRepository.findAll(PageRequest.of(1,10));
		assertThat(pagedUser.getContent().size(), greaterThan(0));
	}
	
	@Test
    public void findByUserId_test() throws Exception {
		
		User user = userRepository.findByUserId(1);
		assertThat(user, hasProperty("name", equalTo("Dominic")));
	}
	
	@Test
    public void findByNameContainingIgnoreCase_test() throws Exception {
		
		Page<User> filteredUsers= userRepository.findByNameContainingIgnoreCase("a", PageRequest.of(1,10));
		assertThat(filteredUsers.getContent().size(), greaterThan(0));
	}
	
}
