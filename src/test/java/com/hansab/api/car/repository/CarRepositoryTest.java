package com.hansab.api.car.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hansab.api.ApiApplication;
import com.hansab.api.car.model.Car;
import com.hansab.api.car.repository.CarRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class CarRepositoryTest {
	
	@Autowired
    private CarRepository carRepository;
	
	@Test
    public void findAll_test() throws Exception {
		
		Page<Car> pagedCar = carRepository.findAll(PageRequest.of(1,10));
		assertThat(pagedCar.getContent().size(), greaterThan(0));
	}
	
	@Test
    public void findByCarId_test() throws Exception {
		
		Car car = carRepository.findByCarId(1);
		System.out.println("----------------------------------------"+car.getNumberPlate());
		assertThat(car, hasProperty("numberPlate", equalTo("513XIS")));
	}
	
	@Test
    public void findByModelContainingIgnoreCase_test() throws Exception {
		
		Page<Car> filteredCars= carRepository.findByModelContainingIgnoreCase("a", PageRequest.of(1,10));
		assertThat(filteredCars.getContent().size(), greaterThan(0));
	}
	
	@Test
    public void findByMakeContainingIgnoreCase_test() throws Exception {
		
		Page<Car> filteredCars= carRepository.findByMakeContainingIgnoreCase("a", PageRequest.of(1,10));
		assertThat(filteredCars.getContent().size(), greaterThan(0));
	}
	
}
