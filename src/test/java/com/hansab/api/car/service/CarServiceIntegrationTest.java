package com.hansab.api.car.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hansab.api.ApiApplication;
import com.hansab.api.car.model.Car;
import com.hansab.api.car.repository.CarRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class CarServiceIntegrationTest {
	
	@TestConfiguration
    static class CarServiceImplTestContextConfiguration {
  
        @Bean
        public CarService carService() {
            return new CarServiceImpl();
        }
    }
 
    @Autowired
    private CarService carService;
 
    @MockBean
    private CarRepository carRepository;
    
    @Before
    public void setUp() {
    	
    	Car v2J9 = new Car();
    	v2J9.setCarId(1L);
    	v2J9.setMake("Isuzu");
    	v2J9.setModel("V2J9");
    	v2J9.setNumberPlate("513XIS");
     
        Mockito.when(carRepository.findByCarId(1))
          .thenReturn(v2J9);
    }
	
    @Test
    public void findByCarId_test() throws Exception {
		
		Car car = carService.findByCarId(1);
		assertThat(car, hasProperty("numberPlate", equalTo("513XIS")));
	}
}
