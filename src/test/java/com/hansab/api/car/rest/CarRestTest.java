package com.hansab.api.car.rest;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.hansab.api.ApiApplication;
import com.hansab.api.car.service.CarService;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ApiApplication.class)
@WebAppConfiguration
public class CarRestTest extends TestCase {
	
	private final String BASE_URI = "http://localhost:8080/api/v1/car/";
	
	private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    CarService carService;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders
                		.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getCars_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URI+"/get").contentType(MediaType.APPLICATION_JSON))
        										 .andExpect(MockMvcResultMatchers.status().isOk())
        										 .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    
    @Test
    public void getPagedCars_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URI+"/get/page/1").contentType(MediaType.APPLICATION_JSON))
        										 .andExpect(MockMvcResultMatchers.status().isOk())
        										 .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    
    public void getCarFromId_test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URI+"/get/1").contentType(MediaType.APPLICATION_JSON))
        										 .andExpect(MockMvcResultMatchers.status().isOk())
        										 .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    public void getFilteredCars_test() throws Exception {
    	
    	String requestJson = "{'query':'a','filter':'make','sortDirection':'ASC'}";
    	
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URI+"/get/page/1").contentType(MediaType.APPLICATION_JSON).content(requestJson))
        										 .andExpect(MockMvcResultMatchers.status().isOk())
        										 .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}
